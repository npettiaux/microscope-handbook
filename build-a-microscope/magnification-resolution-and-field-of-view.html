<!-- magnification-resolution-and-field-of-view.html --> 
<script src="https://rawcdn.githack.com/oscarmorrison/md-page/master/md-page.js"></script><noscript>

# Magnification, Resolution and Field of View

Microscopes are usually specified as having a particular magnification, which is represented as a ratio - typical magnifications range from 4x to 10000x, meaning that the microscope makes things appear a particular number of "times" larger.  Anyone who has asked Richard the seemingly simple question "what magnification is the microscope" will tell you that he's always reluctant to simply reply with a "times number" and this page is an attempt to explain why.  The executive summary is that magnification, on its own, doesn't specify a microscope's capabilities - and different conventions are used for hobby/school microscopes and research/clinical ones.  As people from both backgrounds are likely to ask that question, we deliberately don't quote a "times number", but there are some estimates at the bottom of this page.

Magnification is a ratio of two sizes - in the case of microscopy, the size of your sample (usually small, perhaps tens of microns) and the size of the image you produce (for our microscope, this is generally a digital image on a screen somewhere).  In the digital world of today, it's easy to change the size of an image - resizing the viewing window on your computer screen, moving from a smartphone screen to a projector, and so on.  That means that a 300um wide image displayed as a 30mm wide thumbnail has a magnification of 100x, whereas exactly the same image displayed on a 300mm wide monitor corresponds to a magnification of 1000x.  Project it on a 3 metre wide screen, and it's 10000x.  For a digital microscope, magnification is a somewhat ambiguous quantity.

There are two quantities that encapsulate a pretty good idea of a microscope's performance; they are the [resolution](https://www.microscopyu.com/microscopy-basics/resolution) and the [field of view](https://www.microscopyu.com/microscopy-basics/field-of-view).  Put simply, the resolution is the smallest feature you can see with the microscope, and the field of view is the size of the region of the sample you can see at once.  Both of these are measured in units of distance in the sample, and so they remain the same regardless of the size at which the image is displayed.  The resolution of the microscope is primarily determined by the objective lens, though the illumination of the microscope has an effect here too (e.g. using a condenser lens or a diffuser tends to improve the resolution). For the lenses used in the OpenFlexure Microscope, the approximate resolution and sample-to-sensor magnification is given below:

| Lens           |  Resolution | Sample-to-sensor magnification |
|----------------|-------------|--------------------------------|
| Raspberry Pi Camera v1 | ~2um | 10.6 (40mm stage) |
| Raspberry Pi Camera v2 | ~1.5um | 13.2 (40mm stage) |
| M12 lens in USB camera kit | <2um | 14.9 (65mm stage) |
| Logitech C270 Webcam | ~1.5um | ? |
| 4x, 0.1NA objective | 4.4um | 0.86* (65mm stage) |
| 10x, 0.25NA objective | 1.76um | 2.2* (65mm stage) |
| 20x, 0.4NA objective | 1.1um | 4.3* (65mm stage) |
| 40x, 0.65NA objective | 0.7um | 8.6 (65mm stage) |
| 100x, 1.25NA objective | 0.35um | 21.6 (65mm stage) |

Figures with a * are estimated.  The stage heights are shown because the length of the optics module affects magnification - 40mm and 65mm are the two standard heights so far used in developing the optics.

NB the objective lenses quoted above are typical values for lenses you might find in a research lab.  If you're comparing them to lenses in a microscope kit, make sure to check the numerical aperture (NA) as that's what determines the resolution.  The values in the table above are theoretical, at 532nm.

We won't go into the optics in any detail here - that's for another page (or possibly the excellent [Nikon MicroscopyU](http://www.olympusmicro.com/primer/anatomy/magnification.html)).  However, it's worth understanding a little about the optics, because different people use the quantity of magnification in different ways - and this is surprisingly important.  A traditional compound microscope has two magnification steps: the first uses the objective lens to create an image of the sample.  Typically this is 160mm behind the objective lens, and will be larger than the object being imaged.  The ratio of these two sizes is the magnification of the objective, and it is almost universally between 4x and 100x.  The resolution of the objective is determined by its [Numerical Aperture (NA)](https://www.microscopyu.com/microscopy-basics/numerical-aperture), but most objectives of a given magnification will have a fairly similar NA, so it is common to specify just the magnification of the objective used when taking an image.  In most scientific situations, it is the objective's magnification that is quoted.  Generally, in a microscope, **the objective lens sets the resolution**.  It usually also places an upper limit on the field of view - but that's usually limited by the eyepiece (or camera sensor).

The second magnification step in a microscope is usually the eyepieces.  Generally, in order to allow the image projected by the microscope objective to be viewed comfortably, the eyepiece relays this image to your eye such that it appears to be about 25cm away.  In reality it is generally much closer - so the eyepiece makes the image (which is typically 18-25mm wide after the objective) appear larger.  The most commonly used eyepieces have 10x magnification, but different ratios are available.  Sometimes, the magnification of the eyepieces is included when the magnification of the microscope is specified - e.g. a microscope in a school science kit might advertise 2000x magnification, meaning it has a 40x objective lens and 50x eyepieces.  This is technically correct, but it is not comparable to the magnifications usually quoted in scientific literature.  It is important to remember that the eyepieces cannot improve the resolution of the objective (though in rare circumstances they can degrade it); a 10x objective with a 50x eyepiece will produce an image with much lower resolution than a 100x objective with 5x eyepieces, even though they have the same overall magnification.  Generally, **the eyepiece has an aperture that sets the field of view** - typically it is a disc about 1-2.5cm in diameter in the image formed by the objective (i.e. before any magnification by the eyepieces) corresponding to an apparent image about 25cm wide as viewed through the eyepieces.

Finally, how does all this compare to a digital image formed on a camera sensor?  Typically, a camera used in a microscope will be placed in the image plane formed by the objective.  That means **the field of view is determined by the camera sensor size** divided by the magnification of the objective.  In a typical microscope, the size of a camera sensor is rather smaller than the field of view as seen through the eyepieces; this can have advantages and disadvantages, but is one reason many microscopists favour the eyepieces over a video camera when scanning quickly around the sample.  The optical resolution of the image is still set by the objective, but it is not uncommon to quote the size of one pixel in the image - e.g. a 3280 pixel wide image, corresponding to a field of view of 279um, means each pixel is 85 nanometres wide.  The optical resolution will be much larger than this, because in microscopy **there are usually more pixels on the sensor than we need**.  The difference between two images taken by the same sensor, but saved at e.g. 1 megapixel and 8 megapixel quality, is more or less nothing; the underlying fuzziness (set by the resolution of the objective) means there simply isn't more detail there to capture.

The OpenFlexure microscope is a little different to traditional microscopes with cameras attached - because it doesn't have eyepieces, the design is optimised for a webcam sensor.  As typical webcam sensors are only a few millimetres across, we use a shorter "tube length" so that the image produced by the objective is smaller (and thus fits better onto the small sensor).  Because modern webcams have very small pixels, this doesn't compromise on resolution.  When we use conventional microscope objectives, they are usually combined with a second lens to correct the tube length from around 160mm to around 40mm.  This means that a 40x lens produces an image only 10x larger than the sample.  However, the resolution of the image is unchanged - it simply means that the field of view is larger.  With a 40x lens in a conventional microscope, the Raspberry Pi camera sensor would give a field of view about 90um across; this isn't ideal for most work as it is too zoomed-in, and the image will appear fuzzy as the optical resolution is around 0.7um - the smallest feature you can see is 25 pixels across!  Instead, the reduced tube length means that the image is 426um across; this means you can see more at once, with the same resolution.

A few representative numbers of resolutions and fields of view are below. Underneath are some more detailed comparisons with microscopes commonly found in schools and research labs.

| Microscope                     | Resolution   | Field of View (diagonal)  | Pixel Size   |
|--------------------------------|--------------|-----------------|--------------|
| Basic OpenFlexure Microscope, Raspberry Pi camera & lens | ~1.5um | 349um | 85nm |
| OpenFlexure Microscope with Raspberry Pi camera & 40x objective | 0.7um | 532um | 130nm |
| Basic OpenFlexure USB Microscope, included M12 lens | <2um | 468um | 267nm |
| OpenFlexure USB Microscope with 40x objective | 0.7um | 851um | 465nm |

The sensor sizes for cameras commonly used in the OpenFlexure Microscope are below:

| Sensor  | Native Resolution | Pixel Size  | Sensor Size (XxY) | Sensor Diagonal |
|---------|-------------------|-------------|-------------------|-----------------|
| [Raspberry Pi Camera v1](https://www.raspberrypi.org/products/camera-module/) | 2592x1944 | 1.4um | 3.63x2.72mm | 4.54mm |
| [Raspberry Pi Camera v2](https://www.raspberrypi.org/products/camera-module-v2/) | 2464x3280 | 1.12um | 3.67x2.76mm | 4.60mm |
| Logitech C270 | 1280x720 | 2.8um | 3.58x2.02mm | 4.11mm |
| 5 Megapixel USB Camera (from kit) | 1400x1050 (1) | 4um (1) | 5.6x4.2mm (2) | 7.0mm (2) |
| 6-LED VGA Webcam | 640x480 | 3um (2) | 1.9x1.4mm (2) | 2.4mm (2) |

(1) When we ran this test the 5Mp camera was only running at 1.4Mp.  
(2) This is estimated, rather than taken from the spec sheet of the chip.

To calculate the size of the image, simply divide the the sensor size in mm from the second table by the "sample-to-sensor magnification" value from the first table.  To get the size of one pixel, do the same with the pixel size.  The table below shows the diagonal field-of-view for the OpenFlexure Microscope using a Raspberry Pi camera, the USB 5Mp camera supplied in our kits, and a comparison to the same objective in a lab microscope using a 2/3" video camera and 10x eyepieces.

|   |  PiCam v2 |  Our USB Cam. |  Lab Microscope +  2/3" Sensor |  10x Eyepieces |
|---|-----------|---------------|--------------------------------|----------------|
| PiCam v2 lens (1) | 350um | | | |
| M12 lens (in kit) | 309um | 470um | | |
| 4x Objective | 5350um | 8140um | 2700um | 4250um |
| 10x Objective | 2168um | 3240um | 1080um | 1700um |
| 20x Objective | 1084um | 1628um | 540um | 950um |
| 40x Objective | 542um | 814um | 270um | 425um |
| 100x Objective | 217um | 324um | 108um | 170um |

(1) The Pi Camera lens v2 figure is given for a 40mm stage.  Everything else in the table is for a 65mm stage.  
(2) Objective values are calculated, based on a measurement with the 40x objective.

![Comparison of the field of view for different microscopes with the same objective.](imgs/comparison_of_sensor_sizes.png)

The bottom line is that the basic OpenFlexure Microscope, using the lens from v2 of the Raspberry Pi camera, has a resolution roughly equivalent to a lab microscope with a decent 20x objective lens, but the magnification is closer to that of a 50x objective.  The USB kit (using the lens supplied with the camera) gets a slightly larger field of view, and a slightly coarser resolution.  In a lab microscope equipped with a video camera, the sample-to-sensor magnification is generally the magnification specified on the objective, while the sensor size depends on the camera.  
NB camera sizes given in fractions-of-an-inch are not actual sensor sizes - see the [Sensor Format page on Wikipedia](https://en.wikipedia.org/wiki/Image_sensor_format).  The comparison above quotes values for a 2/3" sensor (10.8mm diagonal), which is a typical size of sensor (fast CMOS sensors are often smaller than this, expensive microscopy cameras can be somewhat larger).  The eyepieces assume a 17mm "field number", which is about typical for 10x oculars.
