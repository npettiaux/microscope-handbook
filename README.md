# microscope-handbook

User guide and maintenance information for the OpenFlexure Microscope, and its variants. It is best viewed in its [rendered form](https://openflexure.gitlab.io/microscope-handbook/).

## Editing the handbook

The handbook is written in [GitHub flavour markdown](https://guides.github.com/features/mastering-markdown/). It uses [md-page](https://github.com/oscarmorrison/md-page) to turn the markdown into HTML.  You will need to paste this script tag at the top of the page, and save it as an `.html`.

```html
<!-- my-page.html --> 
<script src="https://rawcdn.githack.com/oscarmorrison/md-page/master/md-page.js"></script><noscript>
```