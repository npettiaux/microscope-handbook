<!-- index.html --> 
<script src="https://rawcdn.githack.com/oscarmorrison/md-page/master/md-page.js"></script><noscript>

# Getting started developing extensions for the OpenFlexure Microscope remotely with Visual Studio Code

## Introduction

In order to add extra functionality to the OpenFlexure Microscope, a [web API](https://openflexure-microscope-software.readthedocs.io) is available so that developers can easily create extensions in Python without modifying the base code.

>We would love to hear about useful extensions you have created!  Please let us know in an [issue thread](https://gitlab.com/openflexure/openflexure-helpdesk/-/issues/new).

This guide shows you how to set up a development environment using Visual Studio Code so that you can create extensions for the microscope. The Raspberry Pi in the OpenFlexure Microscope is normally not powerful enough to run a development environment *and* the microscope extension software being debugged.  This guide will help you set up remote debugging for the OpenFlexure Microscope so that you can develop extensions on your main computer and debug them on the Raspberry Pi.  

> This guide is for users whose main computer's operating system is Windows 10, although the majority of steps are similar for other operating systems.

## Download the required software

### Install the latest Raspbian-OpenFlexure image

1. If your microscope is not running the latest version of the OpenFlexure software, it is recommended to follow [these handbook instructions](../use-a-microscope/installing-the-software.html) to install the latest version.

> If you already have a copy of the OpenFlexure software installed on your microscope and wish to keep your settings, please follow [these handbook instructions](#retaining-your-settings-from-a-previous-install-of-the-openflexure-software-advanced).


### Download Visual Studio Code

1. Visual Studio Code (VS Code) is a code editor developed by Microsoft for Windows, Linux and MacOS. Download it from the [VS Code website](https://code.visualstudio.com/).
2. Install VS Code on your main computer.

> More information about VS Code can be found on the [VS Code website](https://code.visualstudio.com/docs).


## Connect your main computer to your OpenFlexure Microscope

### Set up your OpenFlexure Microscope

1. The easiest way to set up the microscope is logging in locally.  That allows you to configure WiFi and change your password before you need to use SSH. Follow [these instructions](../use-a-microscope/index.html#starting-the-microsope-for-the-first-time) to start your microscope for the first time.

> You can also complete the setup using an ethernet cable.  If you do that, skip this step and note that WiFi will be disabled until you complete the setup process, and the password will be the default.

### Connect to your microscope with an ethernet cable

1. Connect the ethernet cable to the microscope's Raspberry Pi and your main computer.
2. Your Raspberry Pi will need to be connected to the internet.  If your Raspberry Pi will not be connected to the internet with WiFi, you can share your computer's internet connection over ethernet.  To do so, you need to run the `Internet Connection Sharing (ICS)` service, and share your computer's internet connection to other network users using the `Change adapter options` in the `Network and Internet settings`.

### Test your SSH connection

1. If your computer does not already have one, install an [OpenSSH compatible client](https://code.visualstudio.com/docs/remote/troubleshooting#_installing-a-supported-ssh-client). If you are using Linux, Mac OS, or Windows 10 with October 2018 update or later, you won't need to use a third-party client as you have one built in.
2. In a terminal (i.e. Command Prompt or PowerShell), run the following command:

    `ssh pi@microscope`

3. If you are asked about continue connecting, type `yes`. This will add the Raspberry Pi to the list of known hosts.
4. Enter the password for your `pi` account on the microscope. The default password is `openflexure`.
5. You should now be successfully connected to your Raspberry Pi using SSH.
6. If you didn't complete setup before, run ``sudo raspi-config`` to do so.  This will allow you to set your location, enable and connect to WiFi, and change any other options you want.

> If you don't want to keep entering your password, you can connect to your Raspberry Pi by exchanging SSH keys (passwordless SSH access). There is more information about doing this on the [Raspberry Pi website](https://www.raspberrypi.org/documentation/remote-access/ssh/passwordless.md).

## Set permissions

1. In order to allow development with the Raspberry Pi, you will need to set some permissions so that the `pi` user can edit the server extensions code.  In your terminal (SSH'd to the microscope as before), enter:

    `sudo chmod -R g+w /var/openflexure/`  
    `sudo adduser pi openflexure-ws`  
    `ln -s /var/openflexure/application/openflexure-microscope-server/ ~pi/`

2. If you plan to work on the server application directly, you may want to add or change the remote address in the git repository.  For example, to switch from HTTP to SSH, change to the `openflexure-microscope-server` directory and run:

     `git remote set-url git@gitlab.com/openflexure/openflexure-microscope-server.git`.

## Get the OpenFlexure Software up to date

1. To get the latest development version of the OpenFlexure software, staying in the terminal, run:

    `sudo ofm update`  
    `sudo ofm upgrade -d`

## Set up VS Code for remote development

1. Install the [Remote Development extension pack](https://aka.ms/vscode-remote/download/extension).

> More information about remote development using SSH in VS Code can be found [here](https://code.visualstudio.com/docs/remote/ssh)

## Connect VS Code to your Raspberry Pi

1. Open VS Code and open a new window (`Ctrl+Shift+N`).
2. Open the Command Palette (`F1`), and start typing `Remote-SSH: Connect to Host...`.
3. Click `+ Add New SSH Host`.
4. Type `pi@microscope`.
5. VS Code will then connect to the Raspberry Pi and set itself up. (You may be prompted for your pi user's password.)

## Open the extensions folder on the Raspberry Pi

1. In the `File Explorer` on the right side of the window, click on `Open Folder`. For extensions development, your extensions should be saved at: `/var/openflexure/extensions/microscope_extensions/`. (You may be prompted for your `pi` user's password.)
2. You are now ready to add your extensions to this folder.  You can also use the terminal in VS Code (`Ctrl+Shift+'`), which is automatically SSH'd into the Raspberry Pi,  for example to start and stop the server.
3. In the usual way for VS Code, you may wish to save your **workspace** to keep your settings.

> The documentation for developing extensions for the OpenFlexure Microscope, and an example plugin, can be found [here](https://openflexure-microscope-software.readthedocs.io/en/stable/plugins.html)  

> The API for the OpenFlexure server software can be found [here](https://openflexure-microscope-software.readthedocs.io/en/stable/index.html)  

> The installer includes commands for common tasks when developing extensions for the OpenFlexure server software, such as activating a virtual environment, and starting a development server with real-time debug logging.  The commands can be found [here](https://openflexure.org/projects/microscope/install#managing-the-microscope-server).  

> **WARNING:** Any extensions you develop using this guide will *only* be saved on your Raspberry Pi.  Make sure your code is backed up elsewhere, and consider using version control, such as [git](https://git-scm.com/).  

## Returning to use VS Code for development

In order to return to working on your extension, you don't need to do all these steps again. You can either open your previously saved VS workspace, or:

1. Open VS Code and open a new window (`Ctrl+Shift+N`).
2. Open the Command Palette (`F1`), and start typing `Remote-SSH: Connect to Host...`. (Or click the green button in the bottom left corner).
3. Select `microscope` as the pre-configured SSH host.
4. VS Code will then automatically connect to your Raspberry Pi, where you can return to the extensions folder.

----

## Using VS Code for debugging (Advanced)

TODO

## Retaining your settings from a previous install of the OpenFlexure software (Advanced)

To change your microscope's SD card--for example to install a new image--without losing all your settings, follow these instructions:

1. Log into the microscope (or SSH: `ssh pi@microscope`).
2. Run the following: (to zip the folders)

    `tar -cvjf homedir.tar.bz2 .bashrc .gitconfig .profile .ssh .vim`  
    `cd /var/openflexure`  
    `tar -cvjf ~/openflexure.tar.bz2 data/ extensions/ settings/ logs/`

3. Then from your main computer's terminal run (to copy the folders):

    `scp pi@microscope:homedir.tar.bz2 .\Downloads\`  
    `scp pi@microscope:openflexure.tar.bz2 .\Downloads\`

4. Power off the microscope and swap its SD card (or install a new image on the SD card).
5. Power on the microscope and SSH back into the microscope (`ssh pi@microscope`), and run the following:

    `tar -xvjf homedir.tar.bz2`
    `cd /var/openflexure`
    `sudo -u openflexure-ws tar -xvjf ~/openflexure.tar.bz2`

6. As before, make the OpenFlexure directory user-writable:
    `sudo chmod -R g+w *`

7. Connect your microscope to the internet and switch to the latest development version:
    `sudo ofm update`  
    `sudo ofm upgrade -d`
